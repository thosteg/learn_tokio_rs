use tokio::net::{TcpListener, TcpStream};
use mini_redis::{Connection, Frame};


#[tokio::main]
async fn main() {
    
    // Bind the listener to the address
    let listener = TcpListener::bind("127.0.01:6379").await.unwrap();
    loop {
        let (socket, _) = listener.accept().await.unwrap();
        
        // Could do this: but it is one socket at a time
        // process(socket).await;

        // Spawn new asychronous task (asychronous green thread).
        // Created by passing an async block to tokio::spawn.
        // This returns a JoinHandle.
        // The async block may have a return value.
        //
        // Tasks are the units of execution managed by the scheduler.
        // Spawning the task submits it to the scheduler.
        // Tasks in tokio are leightweight: 64 bytes of memory.
        // Applications should feel free to spawn millions.
        //
        // Tasks must be 'static - not contain reference to data outside.
        // Thats why we "async _move_" variables to be owned by the task.
        //

        // tokio::spawn(async move {
        //    process(socket).await;
        // });

        let handle = tokio::spawn(async move {
            process(socket).await;
            "Done"
        });

        let out = handle.await.unwrap();
        println!("Result of spawn: {}", out);

    
    }    
}

async fn process(socket: TcpStream) {
    // The Connection lets us read/write redis **frames** instead of
    // byte streams. The connection type is defined by mini-redis.

    let mut connection = Connection::new(socket);

    if let Some(frame) = connection.read_frame().await.unwrap() {
        println!("GOT: {:?}", frame);

        // Respond with an error
        let response = Frame::Error("unimplemented".to_string());
        connection.write_frame(&response).await.unwrap();
    }
}
