use tokio::net::{TcpListener, TcpStream};
use mini_redis::{Connection, Frame};


#[tokio::main]
async fn main() {
    
    // Bind the listener to the address
    let listener = TcpListener::bind("127.0.01:6379").await.unwrap();
    loop {
        let (socket, _) = listener.accept().await.unwrap();
        


        let handle = tokio::spawn(async move {
            process(socket).await;
            "Done"
        });

        let out = handle.await.unwrap();
        println!("Result of spawn: {}", out);

    
    }    
}

async fn process(socket: TcpStream) {
    use mini_redis::Command::{self, Get, Set};
    use std::collections::HashMap;

    let mut db = HashMap::new();

    let mut connection = Connection::new(socket);

    while let Some(frame) = connection.read_frame().await.unwrap() {
        let response = match Command::from_frame(frame).unwrap() {
            Set(cmd) => {
                // The value is stored as  Vec<u8>
                db.insert(cmd.key().to_string(), cmd.value().to_vec());
                Frame::Simple("OK".to_string())
            }
            Get(cmd) => {
                if let Some(value) = db.get(cmd.key()) {
                    // Frame::Bulk expects data to be of type Bytes.
                    Frame::Bulk(value.clone().into())
                } else {
                    Frame::Null
                }
            }
            cmd => panic!("unimplemented {:?}", cmd),
        };
        
        // Respond with an error
        connection.write_frame(&response).await.unwrap();
    }
}
