use mini_redis::{client, Result};



#[tokio::main]
pub async fn main() -> Result<()> {
    let mut client = client::connect("127.0.0.1:6379").await?;
       
    let result = client.get("hello").await?;
    match result {
        None => {
            println!("got no value from server");
        }
        Some(result) => {
            println!("got value from server; result={:?}", result);
        }
    }
    Ok(())
}
