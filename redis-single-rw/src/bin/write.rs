use mini_redis::{client, Result};
use rand::prelude::*;


#[tokio::main]
pub async fn main() -> Result<()> {
    let mut rng = rand::thread_rng();
    let sample_vals = ["world", "tiger", "wuffi"];
    let index = rng.gen_range(0..sample_vals.len());

    let key = "hello";
    let chosen_val = sample_vals[index];

    let mut client = client::connect("127.0.0.1:6379").await?;    

    println!("Writing {:?}: {:?}",  key, chosen_val);
    client.set(key, chosen_val.into()).await?;
 
    Ok(())
}
