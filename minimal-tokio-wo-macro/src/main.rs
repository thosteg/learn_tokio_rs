async fn say_world() {
    println!("world");
}

fn main() {
    let rt = tokio::runtime::Runtime::new().unwrap();
    
    rt.block_on(async {
        println!("hello");
    });

    rt.block_on(say_world());
}
