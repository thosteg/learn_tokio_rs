# Following tokio tutorial

Following the following tutorial:
https://tokio.rs/tokio/tutorial/hello-tokio

Existing:

* `minimal-client`:  minimal redis client
* `minimal-tokio`:   run a single async function via tokio
* `minimal-tokio-wo-macro`: as above, but without macro for main function

## Helper: Running minimal complete redis server and client

Installing minimal standalone redis server:

    cargo install mini-redis

Running minimal server:

    mini-redis-server

## Running redis client

Make sure server is running as above:

    cd minimal-client
    cargo run


